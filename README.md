# InteGraal examples

This repository contains an example project that shows the use of [InteGraal](https://gitlab.inria.fr/rules/integraal), an existential rules integration and reasoning system.

## How to run

The project uses Maven to manage dependencies. You can import it into any modern Java IDE, e.g., Eclipse, and run it as a stand-alone Java program.

Alternatively, you can also run it via Maven by executing the following command:
```
mvn compile exec:java
```

If you add another example class and want to run that, you can invoke Maven as follows:
```
mvn compile exec:java -Dexec.mainClass=example.ExampleClassName
```
If not specified, the `mainClass` defaults to `example.InteGraalExample`.

The code requires Java 21 or above to run.

## O2-solubility

> This example needs to be updated and checked

This example is based on a real use case conducted in collaboration with the INRAe institute.

This example shows how to use InteGraal both as an integration and a reasoning system.

You can find the details about this use-case [in the dedicated document](https://gitlab.inria.fr/rules/integraal-examples/-/tree/master/o2Solubility/src/main/resources/demo/o2Solubility).

## Features

You can find some code examples showing how to use the main features of InteGraal in this repository, e;g., chase, rewriting, evaluation, integration.

On these reasoning tasks, some features of the reasoner are available, such as

| Feature                           | Chase | Rewriting | Query Evaluation |
|-----------------------------------|-------|-----------|------------------|
| Computed functions and predicates | ✔     | ✔         | ✔                |
| Negation by failure               | ✔     | ❌         | ✔                |
| Views                             | ✔     | ✔         | ✔                |
