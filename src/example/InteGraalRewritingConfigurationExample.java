package example;

import fr.boreal.backward_chaining.api.BackwardChainingAlgorithm;
import fr.boreal.backward_chaining.core.QueryCoreProcessorImpl;
import fr.boreal.backward_chaining.cover.CoverFunction;
import fr.boreal.backward_chaining.pure.PureRewriter;
import fr.boreal.backward_chaining.pure.rewriting_operator.SingleRuleAggregator;
import fr.boreal.backward_chaining.source_target.SourceTargetRewriter;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.formula.api.FOFormula;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.model.query.api.Query;
import fr.boreal.model.query.impl.UnionFOQuery;
import fr.boreal.model.ruleCompilation.NoRuleCompilation;
import fr.boreal.model.ruleCompilation.api.RuleCompilation;
import fr.boreal.model.ruleCompilation.id.IDRuleCompilation;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * This little example illustrates the use of InteGraal's Rewriting algorithms by showing multiple variants.
 *
 * @author Florent Tornil
 *
 */
public class InteGraalRewritingConfigurationExample {

    /**
     * Rule files are assumed to be in the resources/rules folder.
     * Change this accordingly if you change the project layout
     */
    private static final Path rulesFolder = Path.of("resources", "rules");

    /**
     * Query files are assumed to be in the resources/queries folder.
     * Change this accordingly if you change the project layout
     */
    private static final Path queriesFolder = Path.of("resources", "queries");

    public static void main(String[] args) {

        // Parse rule files (all files in the resources/rules folder with the correct extension)
        RuleBase rulebase = new RuleBaseImpl();
        for(File f : Objects.requireNonNull(rulesFolder.toFile().listFiles())) {
            switch(FilenameUtils.getExtension(f.getName())) {
                case "dlp", "dlgp" -> rulebase.addAll(DlgpParser.parseFile(f.getPath()).rules());
                default -> {} //TODO: error message ? Ignore ?
            }
        }

        // Parse query files (all files in the resources/queries folder with the correct extension)
        Collection<Query> queries = new HashSet<>();
        for(File f : Objects.requireNonNull(queriesFolder.toFile().listFiles())) {
            switch(FilenameUtils.getExtension(f.getName())) {
                case "dlp", "dlgp" -> queries.addAll(DlgpParser.parseFile(f.getPath()).queries());
                default -> {} //TODO: error message ? Ignore ?
            }
        }

        // Use the high level API tu run a rewriting
        // Parameter represents the type of compilation
        // EndUserAPI.rewrite(rulebase, queries, InteGraalKeywords.Algorithms.Parameters.Compilation.NO_COMPILATION);

        BackwardChainingAlgorithm myRewriter;

        // Use the Pure rewriter object to get the default configuration of the rewriting operation
        myRewriter = new PureRewriter();

        // Use the Pure Rewriter with compilation
        // This is the default version
        myRewriter = new PureRewriter(NoRuleCompilation.instance());

        // Use the Pure Rewriter with compilation
        RuleCompilation myCompilation = new IDRuleCompilation();
        myCompilation.compile(rulebase);
        myRewriter = new PureRewriter(myCompilation);

        // Use the Source-Target rewriter to run a rewriting in one step in the case of Source-Target rules
        myRewriter = new SourceTargetRewriter();

        // Use the Pure Rewriter with your own implementation of a rewriting component
        myRewriter = new PureRewriter(new SingleRuleAggregator(), new MyQueryCover(), new QueryCoreProcessorImpl());

        // Don't forget to run the rewriting if constructing it yourself
        for(Query q : queries) {
            UnionFOQuery rewritings = switch (q) {
                case UnionFOQuery uq -> myRewriter.rewrite(uq, rulebase);
                case FOQuery<?> foq -> myRewriter.rewrite(foq, rulebase);
                default -> throw new IllegalStateException("Unexpected value: " + q);
            };

            // Do something with the rewritings
        }
    }
}

// Self implementation of a component
class MyQueryCover implements CoverFunction {

    @Override
    public Set<FOQuery<? extends FOFormula>> cover(Set<FOQuery<? extends FOFormula>> queries) {
        // Update this method with your own behavior
        return queries;
    }
}