package example;

import fr.boreal.forward_chaining.chase.Chase;
import fr.boreal.forward_chaining.chase.ChaseBuilder;
import fr.boreal.forward_chaining.chase.rule_applier.trigger_applier.renamer.TriggerRenamer;
import fr.boreal.io.csv.CSVParser;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.rule.api.FORule;
import fr.boreal.storage.builder.StorageBuilder;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.nio.file.Path;
import java.util.Objects;

/**
 * This little example illustrates the use of InteGraal's Chase algorithm by showing multiple ways to configure it.
 *
 * @author Florent Tornil
 *
 */
public class InteGraalChaseConfigurationExample {

    /**
     * Data files are assumed to be in the resources/data folder.
     * Change this accordingly if you change the project layout
     */
    private static final Path dataFolder = Path.of("resources", "data");

    /**
     * Rule files are assumed to be in the resources/rules folder.
     * Change this accordingly if you change the project layout
     */
    private static final Path rulesFolder = Path.of("resources", "rules");

    public static void main(String[] args) {

        // Parse data files (all files in the resources/data folder with the correct extension)
        FactBase factbase = StorageBuilder.defaultStorage();
        for(File f : Objects.requireNonNull(dataFolder.toFile().listFiles())) {
            switch(FilenameUtils.getExtension(f.getName())) {
                case "dlp", "dlgp" -> factbase.addAll(DlgpParser.parseFile(f.getPath()).atoms());
                case "csv" -> factbase.addAll(new CSVParser(f.getPath()).parse().atoms());
                case "rdf", "ttl", "n3" -> {} //TODO: Add parsing of RDF files
                default -> {} //TODO: error message ? Ignore ?
            }
        }

        // Parse rule files (all files in the resources/rules folder with the correct extension)
        RuleBase rulebase = new RuleBaseImpl();
        for(File f : Objects.requireNonNull(rulesFolder.toFile().listFiles())) {
            switch(FilenameUtils.getExtension(f.getName())) {
                case "dlp", "dlgp" -> rulebase.addAll(DlgpParser.parseFile(f.getPath()).rules());
                default -> {} //TODO: error message ? Ignore ?
            }
        }

        // Use the high level API tu run a chase
        // Parameter represents the type of checker
        //EndUserAPI.saturate(factbase, rulebase, InteGraalKeywords.Algorithms.Parameters.Chase.Checker.SEMI_OBLIVIOUS);

        Chase myChase;

        // Use the Chase builder to get the default configuration of the chase
        myChase = ChaseBuilder.defaultChase(factbase, rulebase);

        // Use the Chase builder with your own configuration
        // The given configuration is the default one
        myChase = ChaseBuilder.defaultBuilder(factbase, rulebase)
                .useGRDRuleScheduler()
                .useGenericFOQueryEvaluator()
                .useFrontierTransformer()
                .useSemiNaiveComputer()
                .useSemiObliviousChecker()
                .useDirectApplication()
                .useFreshNaming()
                .build().orElseThrow();

        // Use the Chase builder to run a stratified chase
        // This configuration stratifies the rulebase to ensure consistency of the reasoning in presence of rules with negated parts
        myChase = ChaseBuilder.defaultBuilder(factbase, rulebase)
                .useStratifiedChase()
                .usePseudoMinimalStratification()
                .build().orElseThrow();

        // Use the ChaseBuilder with you own implementation of a chase component
        myChase = ChaseBuilder.defaultBuilder(factbase, rulebase)
                .setExistentialsRenamer(new MyChaseExistentialRenamer())
                .build().orElseThrow();

        // Don't forget to run the Chase if constructing it with the builder
        myChase.execute();

    }
}

// Self implementation of a component
class MyChaseExistentialRenamer implements TriggerRenamer {

    @Override
    public Substitution renameExitentials(FORule rule, Substitution substitution) {
        // Update this method with your own behavior
        return substitution;
    }
}

