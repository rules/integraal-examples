package example;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.io.csv.CSVParser;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.model.component.InteGraalKeywords;
import fr.boreal.model.kb.api.FactBase;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.Query;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.views.FederatedFactBase;
import fr.boreal.views.builder.ViewBuilder;
import fr.boreal.views.datasource.AbstractViewWrapper;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;

/**
 * This little example illustrates the use of InteGraal with several data sources, rules and queries. It can be modified to create your own InteGraal applications.
 * <br/>
 * For the sake of the example, we split inputs across several files and datasources, although one could get the same data more easily.
 *
 * @author Florent Tornil
 *
 */
public class InteGraalExample {

    /**
     * Data files are assumed to be in the resources/data folder.
     * Change this accordingly if you change the project layout
     */
    private static final Path dataFolder = Path.of("resources", "data");

    /**
     * Rule files are assumed to be in the resources/rules folder.
     * Change this accordingly if you change the project layout
     */
    private static final Path rulesFolder = Path.of("resources", "rules");

    /**
     * Query files are assumed to be in the resources/queries folder.
     * Change this accordingly if you change the project layout
     */
    private static final Path queriesFolder = Path.of("resources", "queries");

    /**
     * View files are assumed to be in the resources/views folder.
     * Change this accordingly if you change the project layout
     */
    private static final Path viewsFolder = Path.of("resources", "views");

    public static void main(String[] args) throws ViewBuilder.ViewBuilderException { //TODO: Handle exceptions

        // Parse data files (all files in the resources/data folder with the correct extension)
        FactBase factbase = StorageBuilder.defaultStorage();
        for(File f : Objects.requireNonNull(dataFolder.toFile().listFiles())) {
            switch(FilenameUtils.getExtension(f.getName())) {
                case "dlp", "dlgp" -> factbase.addAll(DlgpParser.parseFile(f.getPath()).atoms());
                case "csv" -> factbase.addAll(new CSVParser(f.getPath()).parse().atoms());
                case "rdf", "ttl", "n3" -> {} //TODO: Add parsing of RDF files
                default -> {} //TODO: error message ? Ignore ?
            }
        }

        // Parse rule files (all files in the resources/rules folder with the correct extension)
        RuleBase rulebase = new RuleBaseImpl();
        for(File f : Objects.requireNonNull(rulesFolder.toFile().listFiles())) {
            switch(FilenameUtils.getExtension(f.getName())) {
                case "dlp", "dlgp" -> rulebase.addAll(DlgpParser.parseFile(f.getPath()).rules());
                default -> {} //TODO: error message ? Ignore ?
            }
        }

        // Parse query files (all files in the resources/queries folder with the correct extension)
        Collection<Query> queries = new HashSet<>();
        for(File f : Objects.requireNonNull(queriesFolder.toFile().listFiles())) {
            switch(FilenameUtils.getExtension(f.getName())) {
                case "dlp", "dlgp" -> queries.addAll(DlgpParser.parseFile(f.getPath()).queries());
                default -> {} //TODO: error message ? Ignore ?
            }
        }

        // Parse view files (all files in the resources/views folder with the correct extension)
        Collection<AbstractViewWrapper<String, ?>> views = new HashSet<>();
        for(File f : Objects.requireNonNull(viewsFolder.toFile().listFiles())) {
            switch(FilenameUtils.getExtension(f.getName())) {
                case "vd" -> views.addAll(ViewBuilder.createFactBases(f.getPath()));
                default -> {} //TODO: error message ? Ignore ?
            }
        }

        // Create the federation with all the views and the local data
        FederatedFactBase federation = new FederatedFactBase(factbase, views);

        // Execute a reasoning task on the federation
        EndUserAPI.saturate(federation, rulebase, InteGraalKeywords.Algorithms.Parameters.Chase.Checker.SEMI_OBLIVIOUS);

        // Evaluate all the queries on the saturated federation
        for(Query q : queries) {
            System.out.println("\nEvaluating query:" + q);
            Iterator<Substitution> results = EndUserAPI.evaluate(federation, q);
            if(!results.hasNext()) {
                System.out.println("No answer.");
            } else {
                int i = 0;
                while (results.hasNext()) {
                    Substitution result = results.next();
                    System.out.println("Answer " + i + " : " + result);
                    i++;
                }
            }
        }
    }
}
