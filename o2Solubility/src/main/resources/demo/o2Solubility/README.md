# o2-solubility Use Case

This example is based on a real use case conducted in collaboration with the INRAe institute.

This example shows how to use InteGraal both as an integration and a reasoning system.

## Goal

The ultimate goal of this use case is to prepare data that will be used to train a machine learning model to predict a value of `o2-solubility` based on the nutritional composition of food products, with the aim of selecting the best packaging.

In this case, the nutritional composition takes into account five nutrients :
- `salt`
- `sugars`
- `fat`
- `protein`
- `water`

## Problem

The main problem to solve is that the data required for training the machine learning model is not available at a single place, but split between two data sources :

- **AtWeb** which is a triple store accessed via a SparqlEndpoint containing information about food products, `o2-solubility` measurements from scientific papers, and, when mentioned in the scientific paper, some information about the product's nutritional composition.
- **MeatyLab** which is a (private) database accessed via a REST API which references food products and their nutritional composition.

## Data Integration

As we have two heterogeneous data sources, we have to integrate the data between them.

Fortunately, the food products in both data sources have been manually aligned in **AtWeb**, and we have a SPARQL query to retrieve this alignment.

An important aspect of this integration is that we give preference to one of the two data sources when information is available in both. Indeed, as the nutritional composition of the food product in **AtWeb** matches exactly the one measured for the same product in `o2-solubility`, this value will be more precise than the generic value available in **MeatyLab** (which is still better than having no information at all).

## Reasoning

Once the data sources are integrated at the ontological level, we can use the full extent of the existential rules to infer knowledge about our data.

In order to show more usage of the ontology, we extended this use case to answer another question : Which are the "healthy" products ?

We already have all the data needed to answer this question, we just need to add some rules to determine which products are considered "healthy".

## Solution

To solve this use case, we use the usual steps of data integration and reasoning available with InteGraal :

1. We start by defining the relational views that will let us get the data from the data sources.
2. With our views defined, we add the integration layer using existential rules
3. We add an intermediate step to take into account the preferences between data sources using rules with default negation
4. Once we are on the ontology vocabulary (of the integration), we can use the different reasoning strategies available to take into account the rules
5. Finally, we can answer the user queries on the ontology vocabulary.

### 1. Relational Views

The relational views, available in the [mapping.json file](https://gitlab.inria.fr/rules/integraal-examples/-/blob/master/src/main/resources/demo/o2Solubility/views/mappings.json) which follows the [view definition format](https://gitlab.inria.fr/rules/mapping-parser), define how to access the data sources and which native query is associated to each view.

For instance, let's consider the data source **AtWeb** and one of its associated views:

- **View:** `vAtWebNutri`
- **Data Source:** `AtWeb`
- **Query:** to describe this view, we use the following query
```sparql
PREFIX atweb: <http://opendata.inra.fr/resources/hSC9z#>

SELECT ?product ?value ?label
WHERE {
  ?product atweb:composition ?composition .
  ?composition ?component_relation ?component .
  ?component atweb:value ?value .
  ?component atweb:label ?label .
}
```
This query will produce a tuple for each product and nutrient that will result in one element of the view.

In this example, the view `vAtWebNutri` retrieves essential information about the nutritional composition of the food products from **AtWeb**, enabling us to describe how this data can be accessed using a textual format.

A similar approach is applied to other views and data sources within the integration that can be found in [this folder](https://gitlab.inria.fr/rules/integraal-examples/-/tree/master/src/main/resources/demo/o2Solubility/views).

### 2. Integration Layer

The integration layer is defined through multiple [DLGP](https://graphik-team.github.io/graal/doc/dlgp) files, which describe the transition from the vocabulary of the views to the ontology vocabulary.

Continuing with the example of the `vAtWebNutri` view, we define multiple source-to-target rules in the [associated DLGP file](https://gitlab.inria.fr/rules/integraal-examples/-/blob/master/src/main/resources/demo/o2Solubility/reasoning/atWeb_integration.dlgp).

For instance, the `[AtWebFatHighLevelMapping] atWebFat(ProductLabel, Value) :- vAtWebNutri(ProductLabel, Value, "fat").` rule specifies that :
- For all tuple (ProductLabel, Value, Nutrient) retrieved from the `vAtWebNutri` view
- Where `Nutrient` is equal to `fat`,
- A `atWebFat` relation is established between `ProductLabel` and `Value`.

We apply the same principle to the other nutrients and views in the files that can be found in [this folder](https://gitlab.inria.fr/rules/integraal-examples/-/tree/master/src/main/resources/demo/o2Solubility/reasoning).

### 3. Preference Using Negation

To manage preference between the two data sources, default negation is employed. This is defined using [a DLGP file](https://gitlab.inria.fr/rules/integraal-examples/-/blob/master/src/main/resources/demo/o2Solubility/reasoning/preferences.dlgp).

For example, the following rule specifies that :

- For all tuples in the `alignment` relation,
- If we have an information coming from the **MeatyLab** data source
- And not from the **AtWeb** data source,
- The value from **MeatyLab** is chosen.

```
[FatCompletionFromMeatyLab] hasFat(ProductLabel, FatValue, DB)
    :- alignment(ProductLabel, DB, ProductID), meatyLabInformation(DB, ProductID, FatValue, SaltValue, WaterValue, SugarsValue, ProteinValue),
    not atWebFat(ProductLabel, Value).
```

The `hasFat` relation is part of our final ontology vocabulary.

We apply the same principle to the other nutrients. You can find the corresponding rules in [this file](https://gitlab.inria.fr/rules/integraal-examples/-/blob/master/src/main/resources/demo/o2Solubility/reasoning/preferences.dlgp).

Note that the negation used here is stratified, which is for now mandatory for the reasoning in InteGraal.

### 4. Reasoning

For the main goal of our use case, we only need a simple hierarchy to simplify the user query. This is done with the following rule which states that :

- For each nutrient relation about the same product
- A `composition` n-ary relation is established, grouping all the nutrient values
- And a similar `provenance` relation is established grouping all the source of each nutrient value.

```
composition(ProductLabel, FatValue, SaltValue, WaterValue, SugarsValue, ProteinValue),
provenance(ProductLabel, FatSource, SaltSource, WaterSource, SugarsSource, ProteinSource) :-
    hasFat(ProductLabel, FatValue, FatSource),
    hasSalt(ProductLabel, SaltValue, SaltSource),
    hasWater(ProductLabel, WaterValue, WaterSource),
    hasSugars(ProductLabel, SugarsValue, SugarsSource),
    hasProtein(ProductLabel, ProteinValue, ProteinSource).
```

The first step is to determine if the product contains too much of (or is low in) a certain nutrient. For example, the rule `highFatProduct(P) :- product(P), hasFat(P, V, S), V > 17.5.` states that :

- A product `P` is a `highFatProduct`
- If it contains more than `17.5%` of `fat`.

The rule `lowFatProduct(P) :- product(P), hasFat(P, V, S), V < 3.` considers that :

- A product `P` is a `lowFatProduct`
- If it contains less than `3%` of `fat`.

These rules use computed predicates and functions which are intuitive to understand but additional documentation can be found [on the dedicated page](https://gitlab.inria.fr/rules/integraal/-/wikis/User-Documentation/Functions).

The same principle is applied for the `salt` and `sugars` nutrients with corresponding values.

We then compute the proportion of energy given by a product coming from the proteins and tag this product as source or rich in protein according to this proportion.
Finally, we determine that a `healthyProduct` is a product with low fat, salt and sugars that is also rich in protein.

All the ontology rules can be found in [this file](https://gitlab.inria.fr/rules/integraal-examples/-/blob/master/src/main/resources/demo/o2Solubility/reasoning/ontological_hierarchy.dlgp).

### 5. User Query

To conclude this use case, the user can evaluate any query on the ontological vocabulary.

For example, the following query lets us retrieve all the data needed for the training of the machine learning model.

```
?(ProductLabel, SolubilityValue, FatValue, FatSource, SaltValue, SaltSource, WaterValue, WaterSource, SugarsValue, SugarsSource, ProteinValue, ProteinSource) :-
    solubility(ProductLabel, SolubilityValue)
    composition(ProductLabel, FatValue, SaltValue, WaterValue, SugarsValue, ProteinValue),
    provenance(ProductLabel, FatSource, SaltSource, WaterSource, SugarsSource, ProteinSource).
```

And (a preview of) the result :

| ProductLabel                                     | SolubilityValue | FatValue | FatSource | SaltValue | SaltSource | WaterValue | WaterSource | SugarsValue | SugarsSource | ProteinValue | ProteinSource |
|--------------------------------------------------|-----------------|----------|-----------|-----------|------------|------------|-------------|-------------|--------------|--------------|---------------|
| Fish - carbW                                     | 0.084           | 6        | AtWeb     | 0.5       | AtWeb      | 66         | AtWeb       | 0.0         | 2            | 21.0         | 2             |
| Coppa (Italian smoked sausage) - Coppa           | 0.689           | 20.0     | 2         | 25.0      | 2          | 0.5        | 2           | 25.0        | 2            | 4.5          | 2             |
| Beef mince - Beef mince                          | 0.741           | 14.1     | 2         | 0.1       | 2          | 67.2       | 2           | 0.0         | 2            | 20.2         | 2             |
| Fish - carbS                                     | 0.073           | 77       | AtWeb     | 1.2       | AtWeb      | 72         | AtWeb       | 0.0         | 2            | 21.0         | 2             |
| Cooked ham - Cooked Ham                          | 0.000           | 3        | AtWeb     | 32.8      | AtWeb      | 74         | AtWeb       | 0           | AtWeb        | 19           | AtWeb         |
| Bacon - Bacon                                    | 0.838           | 2.6      | 2         | 3.18      | 2          | 70.1       | 2           | 0.2         | 2            | 23.1         | 2             |
| Fish - Recipe 8 control                          | 0.027           | 0.5      | AtWeb     | 0.39      | 2          | 78.1       | AtWeb       | 0.0         | 2            | 17.5         | 2             |
| Fish - controlW                                  | 0.082           | 6        | AtWeb     | 0.5       | AtWeb      | 66         | AtWeb       | 0.0         | 2            | 21.0         | 2             |
| Cheese pressed curd uncooked - Maasdam27fat salt | 20.08           | 26.5     | AtWeb     | 2.7       | AtWeb      | 42         | AtWeb       | 0           | AtWeb        | 27.3         | AtWeb         |
| Hake - Hake                                      | 12.76           | 1.35     | 2         | 0.22      | 2          | 80.0       | 2           | 0.0         | 2            | 17.6         | 2             |
| Turkey meat - Turkey meat                        | 0.793           | 1.88     | 2         | 0.25      | 2          | 75.4       | 2           | 0.14        | 2            | 22.4         | 2             |


## Execution

In order to make a little demonstration of this use case and enable anyone to try it by itself, we added a java main example for this problem that can be found [here](https://gitlab.inria.fr/rules/integraal-examples/-/blob/master/src/main/java/demo/o2Solubility/O2SolubilityINRAEUseCase.java).

This demonstration is not the exact program used to solve the use case because we want everyone to be able to execute it by itself. Therefore, the following modifications have been done :

- We used data sources that run in memory and that are created at the start of the program with the data available in [the dedicated folder](https://gitlab.inria.fr/rules/integraal-examples/-/tree/master/src/main/resources/demo/o2Solubility/data).
  - We used Jena/Fuseki to create a Sparql endpoint in memory representing **AtWeb**
  - We used HSQLDB to create a SQL database in memory representing **MeatyLab**

Once the data sources are created, we can follow the presented steps and :

- Create our `Federation` object from the mapping file
- Execute our reasoning algorithm (here, a saturation approach is chosen) on the different sets of rules
- Evaluate and print the answers to the user query

## Try it yourself

If you want to try this demonstration by yourself, you can use the `o2_demo.jar` jar located at the root of the git using the following command `java -jar o2_demo.jar`.
This is assuming that you have a java version 11 or above available on your computer.

This jar has been generated from the [main java class of the example](https://gitlab.inria.fr/rules/integraal-examples/-/blob/master/src/main/java/demo/o2Solubility/O2SolubilityINRAEUseCase.java) and just executes it.

If you want to try this out in more details, you can clone this repository and import it in your favorite IDE as a Maven Project, and all dependencies will be automatically retrieved.
