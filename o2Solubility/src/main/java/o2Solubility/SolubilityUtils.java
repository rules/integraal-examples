package demo.o2Solubility;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.jena.fuseki.main.FusekiServer;
import org.apache.jena.query.Dataset;
import org.apache.jena.riot.RDFDataMgr;
import org.hsqldb.jdbc.JDBCDataSource;

public class SolubilityUtils {

	private static final String ttlData = String.join("/", "src", "main", "resources", "demo", "o2Solubility", "data", "o2Solubility.ttl");
	private static final String csvData = String.join("/", "src", "main", "resources", "demo", "o2Solubility", "data", "o2Solubility.csv");

	private static FusekiServer server;

	public static void simulateDatasources() {

		Dataset sparqlDataSource = RDFDataMgr.loadDataset(ttlData);
		server = FusekiServer.create()
				.add("/atweb", sparqlDataSource)
				.build();
		server.start();

		JDBCDataSource sqlDataSource = new JDBCDataSource();
		sqlDataSource.setUrl("jdbc:hsqldb:file:o2");
		try(Connection c = sqlDataSource.getConnection()) {
			c.createStatement().execute("SET DATABASE SQL SYNTAX MYS TRUE;");
			c.createStatement().execute("CREATE TEXT TABLE IF NOT EXISTS meatyLab(id INT, database INT, water FLOAT, sugar FLOAT, nacl FLOAT, prot FLOAT, fat FLOAT)");
			c.createStatement().execute("SET TABLE meatyLab SOURCE \""+ csvData + "" + ";fs=,;ignore_first=true\"");
		} catch (SQLException e) {
			throw new RuntimeException("An error occurred while simulating the SQL storage");
		}
	}

	public static void close() {
		server.stop();
	}

}
