package demo.o2Solubility;

import fr.boreal.api.high_level_api.EndUserAPI;
import fr.boreal.io.dlgp.DlgpParser;
import fr.boreal.io.dlgp.ParserResult;
import fr.boreal.model.kb.api.RuleBase;
import fr.boreal.model.kb.impl.RuleBaseImpl;
import fr.boreal.model.logicalElements.api.Substitution;
import fr.boreal.model.query.api.FOQuery;
import fr.boreal.storage.builder.StorageBuilder;
import fr.boreal.views.FederatedFactBase;
import fr.boreal.views.builder.ViewBuilder;

import java.nio.file.Path;
import java.util.Iterator;

public class O2SolubilityINRAEUseCase {

	private static final String reasoningFolder = String.join("/", "data", "demo", "o2Solubility", "reasoning", "");
	private static final Path viewDefinitionFile = Path.of("data","demo", "o2Solubility", "views", "views.vd");

	public static void main(String[] args) throws Exception {

		// Simulate the distant data-sources
		SolubilityUtils.simulateDatasources();

		// Parse view definition file to create the links with the data-sources
		FederatedFactBase federation = new FederatedFactBase(StorageBuilder.defaultStorage(), ViewBuilder.createFactBases(viewDefinitionFile.toString()));

		// Reasoning

		for(String filepath : new String[] {
				reasoningFolder + "atWeb_integration.dlgp",
				reasoningFolder + "meatyLab_integration.dlgp",
				reasoningFolder + "alignment.dlgp",
				reasoningFolder + "preferences.dlgp",
				reasoningFolder + "ontological_hierarchy.dlgp",
		}) {
			ParserResult parsedRules = DlgpParser.parseFile(filepath);
			RuleBase rb = new RuleBaseImpl(parsedRules.rules());

			EndUserAPI.saturate(federation, rb, null, null, null);
		}

		// Query answering

		String queryFile = reasoningFolder + "user_queries.dlgp";

		ParserResult parsedQueries = DlgpParser.parseFile(queryFile);

		for(FOQuery<?> query: parsedQueries.queries()) {
			Iterator<Substitution> results = EndUserAPI.evaluate(federation, query, -1);
			System.out.println(query);
			if(!results.hasNext()) {
				System.out.println("No answer.");
			} else {
				int i = 0;
				while (results.hasNext()) {
					Substitution result = results.next();
					System.out.println("Answer " + i + " : " + result);
					i++;
				}
			}
			System.out.println();
		}

		SolubilityUtils.close();

	}

}
